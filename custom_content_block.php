<?php
/**
 * Plugin Name: Headway: Custom Content Block
 * Description: Modified Headway Custom Content Block
 * Version:     0.2.9
 * Author:      bicarbona
 * Author URI:  https://bicarbona:lucuska@bitbucket.org/bicarbona/headway-content-block
 * License:     GPLv2+
 * Text Domain: custom_content
 * Domain Path: /languages
 * Bitbucket Plugin URI: https://bicarbona:lucuska@bitbucket.org/bicarbona/headway-content-block
* Bitbucket Branch: master
 */


define('CUSTOM_CONTENT_BLOCK_VERSION', '0.1.0');

/**
 * Everything is ran at the after_setup_theme action to insure that all of Headway's classes and functions are loaded.
 **/
add_action('after_setup_theme', 'custom_content_register');
function custom_content_register() {

	if ( !class_exists('Headway') )
		return;

	require_once 'includes/Block.php';
	require_once 'includes/BlockOptions.php';
	//require_once 'design-editor-settings.php';

	return headway_register_block('custom_contentBlock', plugins_url(false, __FILE__));
}

/**
 * Prevent 404ing from breaking Infinite Scrolling
 **/
add_action('status_header', 'custom_content_prevent_404');
function custom_content_prevent_404($status) {

	if ( strpos($status, '404') && get_query_var('paged') && headway_get('pb') )
		return 'HTTP/1.1 200 OK';

	return $status;
}

/**
 * Prevent WordPress redirect from messing up featured board pagination
 */
add_filter('redirect_canonical', 'custom_content_redirect');
function custom_content_redirect($redirect_url) {

	if ( headway_get('pb') )
		return false;

	return $redirect_url;
}