<?php
function headway_content_block_editor_style() {
	
	$body_bg = HeadwayElementsData::get_property('block-content-entry-container', 'background-color', 'ffffff');	
	$body_color = HeadwayElementsData::get_property('block-content-entry-content', 'color', '333333');
	$body_font_family = HeadwayElementsData::get_property('block-content-entry-content', 'font-family', 'helvetica, sans-serif');
	$body_font_size = HeadwayElementsData::get_property('block-content-entry-content', 'font-size', '13');
	$body_line_height = HeadwayElementsData::get_property('block-content-entry-content', 'line-height', '180');

	if ( !($body_hyperlink_color = HeadwayElementsData::get_property('block-content-entry-content-hyperlinks', 'color', null)) )
		$body_hyperlink_color = $body_color;

	return '
		* {
			font-size: ' . $body_font_size . 'px;
			font-family: ' . $body_font_family . ';
			font-style: inherit;
			font-weight: inherit;
			line-height: ' . $body_line_height . '%;
			color: inherit;
		}
		body {
			background: #' . $body_bg . ';
			color: #' . $body_color . ';
			font-size: ' . $body_font_size . 'px;
			font-family: ' . $body_font_family . ';
			line-height: ' . $body_line_height . '%;
		}

		/* Headings */
		h1,h2,h3,h4,h5,h6 {
			clear: both;
		}
		h1,
		h2 {
			color: #000;
			font-size: 15px;
			font-weight: bold;
			margin: 0 0 20px;
		}
		h3, h4, h5, h6 {
			font-size: 10px;
			letter-spacing: 0.1em;
			line-height: 2.6em;
			text-transform: uppercase;
			margin: 0 0 15px;
		}
		hr {
			background-color: #ccc;
			border: 0;
			height: 1px;
			margin: 0 0 15px;
		}

		/* Text elements */
		p {
			margin: 0 0 15px;
		}
		
		/* Lists */
		ul, ol {
			padding: 0 0 0 40px;
			margin: 15px 0;
		}
		
		ul ul, ol ol { margin: 0; } /* Lists inside lists should not have the margin on them. */	

	    ul li { list-style: disc; }
	    ul ul li { list-style: circle; }
	    ul ul ul li { list-style: square; }
	    
	    ol li { list-style: decimal; }
	    ol ol li { list-style: lower-alpha; }
	    ol ol ol li { list-style: lower-roman; }
		
		strong {
			font-weight: bold;
		}
		cite, em, i {
			font-style: italic;
		}
		cite {
			border: none;
		}
		pre {
			background: #f4f4f4;
			font: 13px "Courier 10 Pitch", Courier, monospace;
			line-height: 1.5;
			margin-bottom: 1.625em;
			padding: 0.75em 1.625em;
		}
		code {
			font: 13px Monaco, Consolas, "Andale Mono", "DejaVu Sans Mono", monospace;
		}
		abbr, acronym {
			border-bottom: 1px dotted #666;
			cursor: help;
		}

		/* Links */
		a,
		a em,
		a strong {
			color: #' . $body_hyperlink_color . ';
			text-decoration: underline;
			cursor: pointer;
		}
		a:focus,
		a:active,
		a:hover {
			text-decoration: none;
		}

		/* Alignment */
		.alignleft {
			display: inline;
			float: left;
			margin-right: 1.625em;
		}
		.alignright {
			display: inline;
			float: right;
			margin-left: 1.625em;
		}
		.aligncenter {
			clear: both;
			display: block;
			margin-left: auto;
			margin-right: auto;
		}

.block-type-custom_content div.loop {
	width: 100%;
	float: left;
	clear: both;
	box-sizing: border-box;
	-moz-box-sizing: border-box
}

.block-type-custom_content .entry-title,.block-type-custom_content .archive-title {
	margin: 0 0 5px;
	max-width: 100%;
	word-wrap: break-word
}

.block-type-custom_content .entry-title .post-edit-link {
	display: inline-block;
	font-size: 12px;
	margin: 10px 0 0 15px;
	background: rgba(0,0,0,.6);
	color: rgba(255,255,255,.8) !important;
	padding: 0 10px;
	line-height: 100%;
	vertical-align: middle;
	height: 20px;
	line-height: 20px;
	border-radius: 10px;
	text-shadow: 0 0 1px rgba(0,0,0,.8);
	box-shadow: -1px -1px 0 rgba(255,255,255,.15);
	opacity: 0;
	text-transform: none;
	letter-spacing: 0;
	font-weight: normal;
	position: absolute;
	font-family: helvetica,sans-serif;
	-webkit-font-smoothing: antialiased;
	-moz-transition: opacity 250ms linear;
	-webkit-transition: opacity 250ms linear;
	transition: opacity 250ms linear
}

.block-type-custom_content .post:hover .post-edit-link {
	opacity: .6
}

.block-type-custom_content .entry-title .post-edit-link:hover {
	opacity: 1
}

.block-type-custom_content .archive-title {
	padding: 15px 0 15px;
	border-width: 0
}

.block-type-custom_content .entry-title a,.block-type-custom_content .archive-title a {
	color: inherit;
	font-size: inherit;
	text-decoration: inherit
}

.block-type-custom_content div.entry-content {
	width: 100%;
	margin: 20px 0;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box
}

.block-type-custom_content div.entry-content:first-child {
	margin-top: 0
}

.block-type-custom_content .entry-row {
	float: left;
	width: 100%
}

.block-type-custom_content .entry-row .hentry {
	clear: none
}

.block-type-custom_content .entry-row .hentry:first-child {
	margin-left: 0 !important
}

.block-type-custom_content .hentry {
	clear: both;
	padding: 15px 0 30px;
	margin: 0 0 10px;
	float: left;
	width: 100%;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box
}

.block-type-custom_content a.post-thumbnail {
	display: block
}

.block-type-custom_content a.post-thumbnail img {
	max-width: 100%;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box
}

.block-type-custom_content a.post-thumbnail-left {
	float: left;
	margin: 2px 15px 0 0
}

.block-type-custom_content a.post-thumbnail-right {
	float: right;
	margin: 2px 0 0 15px
}

.block-type-custom_content a.post-thumbnail-above-content {
	margin: 15px 0 -5px
}

.block-type-custom_content a.post-thumbnail-above-title {
	margin: 0 0 15px
}




		';
	
}