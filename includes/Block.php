<?php

class custom_contentBlock extends HeadwayBlockAPI {

    public $id = 'custom_content';
    public $name = 'Custom Content Block';
    public $options_class = 'custom_contentBlockOptions';
    public $description = 'Modified Custom Content Block';
    
	function enqueue_action($block_id) {

	/* CSS */
	// get variable in options 
	//	$scroll_to = $this->get_setting('scroll-to', false);
		
	$scroll_to = parent::get_setting($block_id, 'scroll-to', false);	
		if($scroll_to){
			wp_enqueue_style('headway-scroll-to-css', plugin_dir_url(__FILE__) . '/css/scroll-to.css');		
	/* JS */
	
	//	wp_enqueue_script('headway-scrollto-js', plugin_dir_url(__FILE__) . '/js/jquery.visualNav.min.js', array('jquery'));
	//wp_enqueue_script('easing', plugin_dir_url(__FILE__) . 'http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js', array('jquery'));	

			wp_enqueue_script('headway-scrollto-local', plugin_dir_url(__FILE__) . '/js/jquery.localScroll.min.js', array('jquery'));	

			wp_enqueue_script('headway-scrollto-js', plugin_dir_url(__FILE__) . '/js/jquery.scrollTo.min.js', array('jquery'));
			wp_enqueue_script('headway-scrollto-settings-js', plugin_dir_url(__FILE__) . '/js/scrollTo-settings.js', array('jquery'));	

		}
	}
	
	// public static function init_action($block_id, $block) 
    // {

    // }


    // public static function enqueue_action($block_id, $block, $original_block = null)
    // {

    // }


	// function dynamic_js($block_id, $block = false) {
	
	// 	if ( !$block )
	// 		$block = HeadwayBlocksData::get_block($block_id);
	
	// 	$js = "
	// 	jQuery(document).ready(function() {
			
	// 	jQuery('#scroll-nav').visualNav({
	// 		// content class to get height of the section.
	// 		contentClass      : 'post',
	// 		// css class applied to menu when a link is selected (highlighted).
	// 		selectedClass     : 'current',
	// 		// don't stop animation on mousewheel
	// 		stopOnInteraction : false
	// 	});
	// 	});
	// 	";
	
	// 	return $js;
	
	// }

	public static function dynamic_js($block_id, $block = false) {

		if ( !$block )
			$block = HeadwayBlocksData::get_block($block_id);

			$scroll_to = parent::get_setting($block, 'scroll-to', true);

$js = "
	jQuery(document).ready(function() {
		
	jQuery('#scroll-nav').localScroll({
		lazy: true,
		//target: '.loop',
		 duration:	1000,
		 offset: -30,
	//	currentClass : 'current',
		hash : true, // if true, the hash of the selected link, will appear on the address bar
		easing:'swing',
	     });
	    //return false;

		var hash = window.location.hash;
			if(hash){

				jQuery('a[href=\"' + hash + '\"]').addClass(\"current_page_item\");

			};
			jQuery(\"#scroll-nav a\").click(function(){
			jQuery(\"#scroll-nav a\").removeClass(\"current\");
			jQuery(this).addClass(\"current\");
			});
	});
	";
	//if($scroll_to)
//		return $js;
}

	function init() {
		
		/* Load dependencies */
		require_once  'content-display.php';
		
		/* Set up the comments template */
		add_filter('comments_template', array(__CLASS__, 'add_blank_comments_template'), 5);
		
		/* Set up editor style */
		add_filter('mce_css', array(__CLASS__, 'add_editor_style'));

		/* Add .comment class to all pingbacks */
		add_filter('comment_class', array(__CLASS__, 'add_comment_class_to_all_types'));
		
	}

	public static function add_blank_comments_template() {
		
		return 'comments-template.php';
		
	}

	public static function add_comment_class_to_all_types($classes) {
		
		if ( !is_array($classes) )
			$classes = implode(' ', trim($classes));
				
		$classes[] = 'comment';
		
		return array_filter(array_unique($classes));
		
	}

	public static function add_editor_style($css) {
		
		if ( HeadwayOption::get('disable-editor-style', false, false) )
			return $css;
		
		if ( !current_theme_supports('editor-style') )
			return $css;
			
		if ( !current_theme_supports('headway-design-editor') )
			return $css;

		HeadwayCompiler::register_file(array(
			'name' => 'editor-style',
			'format' => 'css',
			'fragments' => array(
				'headway_content_block_editor_style'
			),
			'dependencies' => array(HEADWAY_LIBRARY_DIR . 'editor-style.php'),
			'enqueue' => false
		));

		return $css . ',' . HeadwayCompiler::get_url('editor-style');

	}

public static function dynamic_css($block_id, $block) {

		$css = '';

		if ( parent::get_setting($block, 'enable-column-layout') ) {

			$gutter_width = parent::get_setting($block, 'post-gutter-width', '20');

			$css = '';

			if ( HeadwayResponsiveGrid::is_enabled() ) {
				$css .= '@media only screen and (min-width: ' . HeadwayBlocksData::get_block_width($block) . 'px) {';
			}

				$css .= '#block-' . $block_id . ' .loop .entry-row .hentry {';

					$css .= 'margin-left: ' . self::width_as_percentage($gutter_width, $block) . '%;';
					$css .= 'width: ' . self::width_as_percentage(self::get_column_width($block), $block) . '%;';

				$css .= '}';

			if ( HeadwayResponsiveGrid::is_enabled() ) {
				$css .= '}';
			}

		}

// Image Hover Effect
		$post_thumbnails_hover_effect = parent::get_setting($block, 'post-thumbnails-hover-effect' , false);

		if ($post_thumbnails_hover_effect){
					$css .= '#block-' . $block_id . ' a.post-thumbnail img{ opacity: 0.8; } #block-' . $block_id . ' a.post-thumbnail:hover img{ opacity: 1; }';

		} else{
					$css .= '#block-' . $block_id . ' a.post-thumbnail img{ opacity: 1; } #block-' . $block_id . ' a.post-thumbnail:hover img{ opacity: 1; }';
		}
// End Image Hover Effect


// DEFAULT CSS 

$css .= '
	.block-type-custom_content div.loop {
		width: 100%;
		float: left;
		clear: both;
		box-sizing: border-box;
		-moz-box-sizing: border-box
	}

	.block-type-custom_content .entry-title,.block-type-custom_content .archive-title {
		margin: 0 0 5px;
		max-width: 100%;
		word-wrap: break-word
	}

	.block-type-custom_content .entry-title .post-edit-link {
		display: inline-block;
		font-size: 12px;
		margin: 10px 0 0 15px;
		background: rgba(0,0,0,.6);
		color: rgba(255,255,255,.8) !important;
		padding: 0 10px;
		line-height: 100%;
		vertical-align: middle;
		height: 20px;
		line-height: 20px;
		border-radius: 10px;
		text-shadow: 0 0 1px rgba(0,0,0,.8);
		box-shadow: -1px -1px 0 rgba(255,255,255,.15);
		opacity: 0;
		text-transform: none;
		letter-spacing: 0;
		font-weight: normal;
		position: absolute;
		font-family: helvetica,sans-serif;
		-webkit-font-smoothing: antialiased;
		-moz-transition: opacity 250ms linear;
		-webkit-transition: opacity 250ms linear;
		transition: opacity 250ms linear
	}

	.block-type-custom_content .post:hover .post-edit-link {
		opacity: .6
	}

	.block-type-custom_content .entry-title .post-edit-link:hover {
		opacity: 1
	}

	.block-type-custom_content .archive-title {
		padding: 15px 0 15px;
		border-width: 0
	}

	.block-type-custom_content .entry-title a,.block-type-custom_content .archive-title a {
		color: inherit;
		font-size: inherit;
		text-decoration: inherit
	}

	.block-type-custom_content div.entry-content {
		width: 100%;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box
	}

	.block-type-custom_content div.entry-content:first-child {
		margin-top: 0
	}

	.block-type-custom_content .entry-row {
		float: left;
		width: 100%
	}

	.block-type-custom_content .entry-row .hentry {
		clear: none
	}

	.block-type-custom_content .entry-row .hentry:first-child {
		margin-left: 0 !important
	}

	.block-type-custom_content .hentry {
		clear: both;
		padding: 15px 0 30px;
		margin: 0 0 10px;
		float: left;
		width: 100%;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box
	}

	.block-type-custom_content a.post-thumbnail {
		display: block
	}

	.block-type-custom_content a.post-thumbnail img {
		max-width: 100%;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box
	}

	.block-type-custom_content a.post-thumbnail-left {
		float: left;
		margin: 2px 15px 0 0
	}

	.block-type-custom_content a.post-thumbnail-right {
		float: right;
		margin: 2px 0 0 15px
	}

	.block-type-custom_content a.post-thumbnail-above-content {
		margin: 15px 0 -5px
	}

	.block-type-custom_content a.post-thumbnail-above-title {
		margin: 0 0 15px
	}
	';

		return $css . "\n";
	
}


	static function get_column_width($block) {

			$block_width = HeadwayBlocksData::get_block_width($block);

			$columns = parent::get_setting($block, 'posts-per-row', '2');
			$gutter_width = parent::get_setting($block, 'post-gutter-width', '20');

			$total_gutter = $gutter_width * ($columns-1);

			$columns_width = (($block_width - $total_gutter) / $columns);

			return $columns_width; 
		}

		/* To make the layout responsive
		 * Works out a percentage value equivalent of the px value 
		 * using common responsive formula: target_width / container_width * 100
		 */	
		static function width_as_percentage($target = '', $block) {
			$block_width = HeadwayBlocksData::get_block_width($block);
			
			if ($block_width > 0 )
				return ($target / $block_width)*100;

			return false;
		}

function setup_elements() {
		
		$this->register_block_element(array(
			'id' => 'entry-container-hentry',
			'name' => 'Entry Container',
			'selector' => '.hentry',
			'properties' => array('background', 'borders', 'padding', 'corners', 'box-shadow')
		));


		$this->register_block_element(array(
				'id' => 'page-container',
				'name' => 'Page Entry Container',
				'parent' => 'entry-container-hentry',
				'selector' => '.type-page'
			));

			$this->register_block_element(array(
				'id' => 'entry-container',
				'name' => 'Post Entry Container',
				'parent' => 'entry-container-hentry',
				'selector' => '.type-post'
			));

		$this->register_block_element(array(
			'id' => 'entry-row',
			'name' => 'Entry Row',
			'selector' => '.entry-row'
		));
		
		$this->register_block_element(array(
			'id' => 'title-before-text',
			'name' => 'Title Before',
			'selector' => '.title-before',
			'states' => array(
				// 'Hover' => '.title-before:hover', 
				// 'Clicked' => '.title-before:active'
			),
			'inherit-location' => 'default-heading'
		));

		$this->register_block_element(array(
			'id' => 'title-before-text-span',
			'name' => 'Title Before Span',
			'selector' => '.title-before span',
			'states' => array(
				// 'Hover' => '.title-before:hover', 
				// 'Clicked' => '.title-before:active'
			),
			'inherit-location' => 'default-heading'
		));


		$this->register_block_element(array(
			'id' => 'title',
			'name' => 'Title',
			'selector' => '.entry-title',
			'states' => array(
				'Hover' => '.entry-title:hover', 
				'Clicked' => '.entry-title:active'
			),
			'inherit-location' => 'default-heading'
		));
		


		$this->register_block_element(array(
			'id' => 'archive-title',
			'name' => 'Archive Title',
			'selector' => '.archive-title',
			'inherit-location' => 'default-heading'
		));
		

		$this->register_block_element(array(
			'id' => 'entry-content-block',
			'name' => 'Entry Content',
			'description' => '',
			'selector' => 'div.entry-content',
			'inherit-location' => 'default-text'
		));


		$this->register_block_element(array(
			'id' => 'entry-content',
			'name' => 'Body Text',
			'description' => 'All text including &lt;p&gt; elements',
			'selector' => 'div.entry-content p',
			'parent'=> 'entry-content-block',
			'inherit-location' => 'default-text'
		));

		$this->register_block_element(array(
			'id' => 'blockquote',
			'name' => 'Blockquote',
			'selector' => 'div.entry-content blockquote',
			//'inherit-location' => 'default-text'
		));

			$this->register_block_element(array(
				'id' => 'blockquote-p',
				'name' => 'Blockquote P',
				'selector' => 'div.entry-content blockquote p',
				'parent' => 'blockquote'
			));


		$this->register_block_element(array(
			'id' => 'entry-content-hyperlinks',
			'name' => 'Body Hyperlinks',
			'selector' => 'div.entry-content a',
			'properties' => array('fonts'),
			'inherit-location' => 'default-text',
			'states' => array(
				'Hover' => 'div.entry-content a:hover', 
				'Clicked' => 'div.entry-content a:active'
			)
		));

		$this->register_block_element(array(
			'id' => 'entry-content-images',
			'name' => 'Body Images',
			'selector' => 'div.entry-content img',
			'properties' => array('background', 'borders', 'padding', 'corners', 'box-shadow')
		));
		
		$this->register_block_element(array(
			'id' => 'entry-meta',
			'name' => 'Meta',
			'selector' => 'div.entry-meta',
			'inherit-location' => 'default-text'
		));

			$this->register_block_element(array(
				'id' => 'entry-meta-above',
				'name' => 'Meta Above Content',
				'selector' => 'div.entry-meta-above',
				'parent' => 'entry-meta',
				'inherit-location' => 'default-text'
			));

			$this->register_block_element(array(
				'id' => 'entry-meta-below',
				'name' => 'Meta Below Content',
				'selector' => 'div.entry-utility-below',
				'parent' => 'entry-meta',
				'inherit-location' => 'default-text'
			));

			$this->register_block_element(array(
				'id' => 'entry-meta-author',
				'name' => 'Author Avatar Image',
				'selector' => '.avatar',
				'parent' => 'entry-meta',
				'inherit-location' => 'default-text'
			));

			$this->register_block_element(array(
				'id' => 'entry-date',
				'name' => 'Post Entry Date',
				'parent' => 'entry-meta',
				'selector' => '.entry-date'
			));
		
		$this->register_block_element(array(
			'id' => 'heading',
			'name' => 'Heading',
			'selector' => 'div.entry-content h3, div.entry-content h2, div.entry-content h1',
			'inherit-location' => 'default-heading'
		));

			$this->register_block_element(array(
				'id' => 'heading-h1',
				'parent' => 'heading',
				'name' => 'H1',
				'selector' => 'h1.entry-title',
				'inherit-location' => 'block-content-heading',
				'parent' => 'heading'
			));

			$this->register_block_element(array(
				'id' => 'heading-h2',
				'parent' => 'heading',
				'name' => 'H2',
				'selector' => 'div.entry-content h2',
				'inherit-location' => 'block-content-heading'
			));

			$this->register_block_element(array(
				'id' => 'heading-h3',
				'parent' => 'heading',
				'name' => 'H3',
				'selector' => 'div.entry-content h3',
				'inherit-location' => 'block-content-heading'
			));
		
		$this->register_block_element(array(
			'id' => 'sub-heading',
			'name' => 'Sub Heading',
			'selector' => 'div.entry-content h4, div.entry-content h5',
			'inherit-location' => 'default-sub-heading'
		));

			$this->register_block_element(array(
				'id' => 'sub-heading-h4',
				'parent' => 'sub-heading',
				'name' => 'H4',
				'selector' => 'div.entry-content h4',
				'inherit-location' => 'block-content-sub-heading'
			));

			$this->register_block_element(array(
				'id' => 'sub-heading-h5',
				'parent' => 'sub-heading',
				'name' => 'H5',
				'selector' => 'div.entry-content h5',
				'inherit-location' => 'block-content-sub-heading'
			));
			$this->register_block_element(array(
			'id' => 'content-ul-lists',
			'name' => 'Unordered Lists',
			'description' => '&lt;UL&gt;',
			'selector' => 'div.entry-content ul',
			'properties' => array('fonts', 'background', 'borders', 'padding', 'corners', 'box-shadow'),
		));


			$this->register_block_element(array(
				'id' => 'content-ul-list-item',
				'name' => 'Unordered List Items',
				'description' => '&lt;LI&gt;',
				'selector' => 'div.entry-content ul li',
				'properties' => array('fonts', 'lists', 'background', 'borders', 'padding', 'corners', 'box-shadow'),
			));

			$this->register_block_element(array(
			'id' => 'content-ol-lists',
			'name' => 'Ordered Lists',
			'description' => '&lt;OL&gt;',
			'selector' => 'div.entry-content ol',
			'properties' => array('fonts', 'background', 'borders', 'padding', 'corners', 'box-shadow'),
		));

			$this->register_block_element(array(
				'id' => 'content-list-item',
				'name' => 'Ordered List Items',
				'description' => '&lt;LI&gt;',
				'selector' => 'div.entry-content ol li',
				'properties' => array('fonts', 'lists', 'background', 'borders', 'padding', 'corners', 'box-shadow'),
			));

		$this->register_block_element(array(
			'id' => 'post-thumbnail',
			'name' => 'Featured Image',
			'selector' => '.block-type-custom_content a.post-thumbnail img',
			'properties' => array('background', 'borders', 'padding', 'corners', 'box-shadow')
		));
		
		$this->register_block_element(array(
			'id' => 'more-link',
			'name' => 'Continue Reading Button',
			'selector' => 'div.entry-content a.more-link',
			'states' => array(
				'Hover' => 'div.entry-content a.more-link:hover',
				'Clicked' => 'div.entry-content a.more-link:active'
			)
		));
		
		$this->register_block_element(array(
			'id' => 'loop-navigation-link',
			'name' => 'Loop Navigation Button',
			'selector' => 'div.loop-navigation div.nav-previous a, div.loop-navigation div.nav-next a',
			'states' => array(
				'Hover' => 'div.loop-navigation div.nav-previous a:hover, div.loop-navigation div.nav-next a:hover',
				'Clicked' => 'div.loop-navigation div.nav-previous a:active, div.loop-navigation div.nav-next a:active'
			)
		));

		$this->register_block_element(array(
			'id' => 'comments-wrapper',
			'name' => 'Comments',
			'selector' => 'div#comments'
		));
		
		$this->register_block_element(array(
			'id' => 'comments-area',
			'name' => 'Comments Area',
			'selector' => 'ol.commentlist',
			'properties' => array('background', 'borders', 'padding', 'corners', 'box-shadow'),
			'parent' => 'comments-wrapper'
		));
		
		$this->register_block_element(array(
			'id' => 'comments-area-headings',
			'name' => 'Comments Area Headings',
			'selector' => 'div#comments h3',
			'parent' => 'comments-wrapper'
		));

		$this->register_block_element(array(
			'id' => 'comment-container',
			'name' => 'Comment Container',
			'selector' => 'li.comment',
			'properties' => array('background', 'borders', 'padding', 'corners', 'box-shadow'),
			'parent' => 'comments-wrapper'
		));

		$this->register_block_element(array(
			'id' => 'comments-textarea',
			'name' => 'Add Comment Textarea',
			'selector' => '#comment',
			'parent' => 'comments-wrapper'
		));
		
		$this->register_block_element(array(
			'id' => 'comment-author',
			'name' => 'Comment Author',
			'selector' => 'li.comment .comment-author',
			'parent' => 'comments-wrapper'
		));
		
		$this->register_block_element(array(
			'id' => 'comment-meta',
			'name' => 'Comment Meta',
			'selector' => 'li.comment .comment-meta',
			'parent' => 'comments-wrapper'
		));

		$this->register_block_element(array(
			'id' => 'comment-meta-count',
			'name' => 'Comment Meta Count',
			'selector' => 'a.entry-comments',
			'parent' => 'comments-wrapper'
		));
		
		$this->register_block_element(array(
			'id' => 'comment-body',
			'name' => 'Comment Body',
			'selector' => 'li.comment .comment-body p',
			'properties' => array('fonts'),
			'parent' => 'comments-wrapper'
		));
		
		$this->register_block_element(array(
			'id' => 'comment-reply-link',
			'name' => 'Comment Reply Link',
			'selector' => 'a.comment-reply-link',
			'states' => array(
				'Hover' => 'a.comment-reply-link:hover',
				'Clicked' => 'a.comment-reply-link:active'
			),
			'parent' => 'comments-wrapper'
		));

		$this->register_block_element(array(
			'id' => 'comment-form-input-label',
			'name' => 'Comment Form Input Label',
			'selector' => 'div#respond label',
			'properties' => array('fonts'),
			'parent' => 'comments-wrapper'
		));

		$this->register_block_element(array(
			'id' => 'cell',
			'name' => 'Cell',
			'selector' => '.cell',
		));

			$this->register_block_element(array(
				'id' => 'cell1',
				'name' => 'Cell 1',
				'selector' => '.cell1',
				'parent' => 'cell'
			));

			$this->register_block_element(array(
				'id' => 'cell2',
				'name' => 'Cell 2',
				'selector' => '.cell2',
			'parent' => 'cell'
			));

			$this->register_block_element(array(
				'id' => 'cell3',
				'name' => 'Cell 3',
				'selector' => '.cell3',
				'parent' => 'cell'
			));

			$this->register_block_element(array(
				'id' => 'cell4',
				'name' => 'Cell 4',
				'selector' => '.cell4',
				'parent' => 'cell'
			));

			$this->register_block_element(array(
				'id' => 'cell5',
				'name' => 'Cell 5',
				'selector' => '.cell5',
				'parent' => 'cell'
			));
		
			$this->register_block_element(array(
				'id' => 'cell6',
				'name' => 'Cell 6',
				'selector' => '.cell6',
				'parent' => 'cell'
			));

			$this->register_block_element(array(
				'id' => 'cell7',
				'name' => 'Cell 7',
				'selector' => '.cell7',
				'parent' => 'cell'
			));
	}

	public function heloo()
	{
		# code...

		echo "helooo";
	}
	
	function content($block) {

		$title_before = parent::get_setting($block, 'title-before-text');
		$html_tag = parent::get_setting($block, 'title-html-tag', 'h1');
		$special_span = parent::get_setting($block, 'special-span', false);

		if($special_span){
			$special_span_before = '<span>';
			$special_span_after = '</span>';
		} else {
			$special_span_before = '';
			$special_span_after = '';
		}

		if(!$linked)
			$main_title = '<' . $html_tag . ' class="title-before">'.$special_span_before. $title_before. $special_span_after.'</' . $html_tag . '>';

		/**
		// TODO pridat linked napr link na kategoriu? .... atd
		***/	

		if($linked){
			$main_title = '<' . $html_tag . ' class="title-before">'.$special_span_before.'
			<a href="'. get_post_permalink($id) .'" rel="bookmark" title="'. the_title_attribute (array('echo' => 0) ) .'">'. $title_before .'</a>'.$special_span_after.'
			</' . $html_tag . '>';
			$main_title = '<' . $html_tag . ' class="title-before">'.$special_span_before. $title_before. $special_span_after.'</' . $html_tag . '>';
		}
		if($title_before){
			echo $main_title;
		}
		
		$content_block_display = new HeadwayCustomContentBlockDisplay($block);
		$content_block_display->display();

?>

<?php 

/**
SCROLL TO

**/

// http://jsfiddle.net/P3kka/
// http://jsfiddle.net/ARTsinn/ADcbG/
// http://www.aquatapplumbing.com.au/jQuery.LocalScroll/jQuery.LocalScroll.html#section1
// 	http://www.webdesignweb.fr/sources/scrollto/index.html
// http://www.adriantomic.se/development/jquery-localscroll-tutorial/
// http://www.webdesignweb.fr/web/fonction-scrollto-pour-les-ancres-nommees-jquery-820
// jQuery(document).ready(function() {
	
// 	// TOC, shows how to scroll the whole window
// 	jQuery('#scroll-nav a').click(function(){
// 		$.scrollTo( this.hash, 1500, { easing:'swing' });
// 		return false;
// 	});
// });
// http://easings.net/

// // Many of these defaults, belong to jQuery.ScrollTo, check it's demo for an example of each option.
// // @see http://flesler.demos.com/jquery/scrollTo/
// // The defaults are public and can be overriden.
// $localScroll.defaults = {
// duration:1000, // How long to animate.
// axis:'y', // Which of top and left should be modified.
// offset: -175,
// event:'click', // On which event to react.
// stop:true, // Avoid queuing animations
// target: window, // What to scroll (selector or element). The whole window by default.
// reset: true // Used by $.localScroll.hash. If true, elements' scroll is resetted before actual scrolling
// /*
// lock:false, // ignore events if already animating
// lazy:false, // if true, links can be added later, and will still work.
// filter:null, // filter some anchors out of the matched elements.
// hash: false // if true, the hash of the selected link, will appear on the address bar.
// */
// };



     // You can modify any default like this
// $.localScroll.defaults.axis = 'xy';

// // Scroll initially if there's a hash (#something) in the url 
// $.localScroll.hash({
//         target: '#content', // Could be a selector or a jQuery object too.
//         queue:true,
//         duration:1500
// });

/**
 * NOTE: I use $.localScroll instead of $('#navigation').localScroll() so I
 * also affect the >> and << links. I want every link in the page to scroll.
 */
// $.localScroll({
// target: '#content', // could be a selector or a jQuery object too.
// queue:true,
// duration:1000,
// hash:true,
// onBefore:function( e, anchor, $target ){
// // The 'this' is the settings object, can be modified
// },
// onAfter:function( anchor, settings ){
// // The 'this' contains the scrolled element (#content)
// }
// });
// });//@ sourceURL=pen.js


	}

}